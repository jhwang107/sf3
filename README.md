# SimpleFlow 3
A Smart Reflow Oven Controller with Dual MAX31856 Thermocouple Sensor

(c) 2017-2019 [James Hwang](mailto:james.hwang@sylmac.cc) / [Sylmac Labs](http://www.sylmac.cc)

# Hardware Features
1. Two MAX31856 thermocouple converter built onboard
2. FTDI FT231X chip for most robust USB-Serial connection
3. Atmega1284P with greatly increased programming space for later date improvements
4. Omega PCC-SMP style thermocouple mini jack
5. Large 1604 LCD display
6. Big Rotary Encoder Dial for fast and easy navigation
7. Two button switches
8. 6-pin UART connects directly to HC-06 Bluetooth or other modules alike
9. 1x ZeroCross Detection input for AC Phase Angle Control (requires input from a separate ZeroCross Detection module)
10. 4x Open Drain (200mA each) Configurable Outputs
11. 1x LED status for each of the open drain outputs
12. 1x Servo Control Output for Auto-Opening of Oven door at end of Reflow Profile
13. 1x Buzzer Output for Sound Alerts
14. +5VDC power input with reverse protection (suggest to use at least 5V/2A power supply)
15. Dimension is 92 x 72 (mm), Depth is about 25mm, Rotary encoder dial protudes about 15mm

# Software Functions
1. Multiphase Bake function, configurable to more than 30 days
2. Multiphase Reflow function, for better RSS (Ramp-Soak-Spike) profile control
3. Stores 9 Preset Reflow profile for quick access
4. Per Second Heatrate display and log
5. Full PID control
6. Intelligent IndieDev Ramp Engine for Heating Control
7. Outputs can be individually configured as: TOP heat, Bottom heat, Convection FAN, or Cooling FAN
8. Log outputs via serial ports
9. Adjust temperature/time setting while midway into Bake profile
10. Increase Reflow time while midway into Reflow profile to increase yield
11. On-the-Fly heat and time hysteresis compensation

# Steps to Update SF3 Firmware
1. Connect SF3 to USB port
2. note down the Comm port assigned
3. Open avrdudess.exe
4. set MCU  = Atmega1284p
5. set Programmer = Arduino
6. set COM Port
6. set Baud rate = 115200
7. choose the FW file to update with
8. Click "Program!" button to start

![](update_tools/update_using_avrdudess.png)